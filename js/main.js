angular.module('Calendar', [])
  .directive("calendarwidget", function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'templates/calendar-widget.html',
      scope: {
        selectedDate: '=selectedDate'
      },
      link: function(scope, element, attrs) {
        scope.today = function() {
          this.selectedDate = 18;
        }
      }
    }
  })
  .directive("filter", function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'templates/filter.html',
      link: function(scope, element, attrs) {
        scope.reset = function() {
          scope.searchText = "";
        }
      }
    }
  })
  .filter("dateFilter", function() {
      return function(input, format) {
          moment.lang(this.$parent.locale);
          return moment(input).format(format);
      }
  });

function Cldr($scope) {

  $scope.locale = "en";  
  $scope.selectedDate = 17;

  $scope.changeLocale = function() {
    moment.lang($scope.locale);
  }
}